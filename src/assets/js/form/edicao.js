var modal = $("#modal-editor");

var liveEditable = function(type, obj) {

    modal.hide();

    $.ajax({
        method: "get",
        url: obj.attr("data-url"),
        success: function(data){
            modal.find(".modal-body").html(data);

            if(obj.hasClass("new")) {
                if(obj.attr("data-edit-capa")) modal.find("form.ajax").append("<input type=\"hidden\" name=\"multi_ed_capa\" value=\""+obj.attr("data-edit-capa")+"\">");
                else modal.find("form.ajax").append("<input type=\"hidden\" name=\"multi_ed_materia\" value=\""+obj.attr("data-edit-materia")+"\">");
            }

            FormSliderSwitcher.init('.modal');
        }
    });

    if (type == "modulo") {
        modal.find(".modal-title").html("Editando Conteudo");
    } else {
        modal.find(".modal-title").html("Editando Linha");
    }

    modal.show();

}

var remove = function(type, obj) {

    $.ajax({
        method: "delete",
        url: obj.attr("data-url"),
        data:{"_token":obj.attr("data-token")},
        success: function(data){
            $("iframe")[0].contentWindow.location.reload();
        }
    });

}

var submitForm = function(obj) {

  var data = new FormData(obj[0]);
  var url = obj.attr("action");

  $.ajax({
    url: url,
    type: 'POST',
    data: data,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.status == "success") {
            modal.hide();
        } else {
            modal.find(".modal-body").html(data);
        }

        $("iframe")[0].contentWindow.location.reload();
    }
  });

}

modal.find(".close-modal").click(function(){

    modal.hide();

});

var binds = function() {
    modal.find(".save").click(function(){
        var itemForm = modal.find("form.ajax");
        itemForm.submit(function(e){
            e.preventDefault();
        });

        submitForm(itemForm);
    });
}();
