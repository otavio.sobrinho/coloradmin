@extends('admin::base.container')

<?php
use \locknload\Admin\Macros;
use \locknload\Crud\Helper;
use \locknload\Crud\Fields;

$lastKey = "";
?>

@section('conteudo')

    <div id="content" class="content">

        <h1 class="page-header"> Editar {!!Helper::translateField($data['class'])!!} </h1>

        <div class="col-md-12" style="height:92%;">

            <div class="panel panel-inverse" data-sortable-id="form-stuff-2" style="height:100%;">

                <div class="panel-heading">

                    <h4 class="panel-title">Live editor</h4>

                </div>

                <div class="panel-body" style="height:92%;">

                        <iframe src="/liveeditor/{{studly_case($data['class'])}}/{{$data['page']->id}}/{{$data['type']}}" width="100%" height="100%" style="border:none;overflow:auto;"></iframe>

                </div>

            </div>

        </div>

        @include('admin::crud.bancoImagensContainer')

        @include('admin::crud.modalcontainer')

    </div>

@stop
