<?php

use \locknload\Admin\Macros;
use \locknload\Crud\Helper;
use \locknload\Crud\Fields;

?>

<style>

.item{
    text-align: center;
}

</style>

@if(isset($data['conteudo']))

  {{ Form::open(['url' => '/api/liveeditor/save/ed_conteudo', 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal form-bordered ajax', 'role' => 'form', 'style' => 'overflow:hidden;']) }}
    <input type="hidden" name="id" value="{{$data['conteudo']->id}}">

    @foreach($data['struct'] as $strc)
      <?php $f = $data['obj']; $g=$strc['field']; $c=preg_replace("/b_/","",$strc['field']);?>
      @if($f->$g == 1)
        @if($c != 'texto')
            <div class="form-group col-md-6" style="min-height: 72px;overflow: hidden;">
                <label class="control-label col-md-4 ui-sortable">{{$c}}</label>
                <div class="col-md-8 ui-sortable">
                    <input type="text" name="{{$c}}" class="form-control" value="{{$data['conteudo']->$c}}" placeholder="{{$c}}" style="width:100%;" />
                </div>
            </div>
        @else
            <div class="form-group col-md-12" style="min-height: 72px;overflow: hidden;">
                <label class="control-label col-md-2 ui-sortable">{{$c}}</label>
                <div class="col-md-10 ui-sortable">
                    <div class="form-group col-md-12">
                        <textarea name="{{$c}}" rows="10" class="wysihtml5">{{$data['conteudo']->$c}}</textarea>
                    </div>
                </div>
            </div>
        @endif
       @endif
    @endforeach
  {{ Form::close() }}

  <script>
    $(".wysihtml5").wysihtml5();
  </script>
@else
    @if($data['obj'] || $data['models'] )

        {{ Form::open(['url' => '/api/liveeditor/model/ed_linha/ed_modulo/', 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal form-bordered ajax', 'role' => 'form', 'style' => 'overflow:hidden;']) }}

            <input type="hidden" name="id_line" value="{{$data['idline']}}">
            <input type="hidden" name="column" value="{{$data['column']}}">

            @foreach($data['models'] as $model)
                <div class="col-md-4 item">
                    <div>
                        @if($data['obj']['id'] == $model->id)
                            <input type="radio" name="id_model" value="{{$model->id}}" checked>
                        @else
                            <input type="radio" name="id_model" value="{{$model->id}}">
                        @endif
                        <img src="{{$model->image->imagem}}" width="100px"/>
                    </div>
                    <div>
                        {{$model->name}}
                    </div>
                </div>
            @endforeach

        {{ Form::close() }}

    @endif
@endif
