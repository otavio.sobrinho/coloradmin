<?php

use \locknload\Admin\Macros;
use \LocknLoad\Crud\Fields;

?>

<div class="row">

    {{ Form::open(['url' => '/api/liveeditor/content/ed_modulo/'.$data['obj']['id'], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal form-bordered ajax', 'role' => 'form']) }}

        @foreach ($data['struct'] as $field)

            @if(Fields::isBool($field) && $data['obj'][$field['field']] == 1)

                <div class="col-md-6" style="margin:15px 0;">
                    <label class="control-label col-md-4">
                        {!!Macros::generateLabel(isset($field['label'])? $field['label']:$field['field'],'NO')!!}
                    </label>

                    <div class="col-md-8">
                            <input type="text" class="form-control" placeholder="" name="{{preg_replace("/b_/","",$field['field'])}}" value="{{$data['content'][preg_replace("/b_/","",$field['field'])]}}" style="width:100%;">
                    </div>
                </div>

            @endif

        @endforeach

    {{Form::close()}}

</div>
