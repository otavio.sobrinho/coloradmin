@extends('admin::base.container')

@section('script')
    <script>
        FormSliderSwitcher.init();
        $("form .btn.btn-remove").click(function(e){
            e.preventDefault();

            var obj = $(e.target).prop("tagName") == "A" ? $(e.target) : $(e.target).parents("a");
            var form = obj.parents("form");

            $.ajax({
                url:  form.attr("action"),
                type: 'DELETE',
                data: form.serialize(),
                success: function(result) {
                    location.reload();
                }

            });

        });
</script>
@stop

@section('conteudo')
<div id="content" class="content">

<h1 class="page-header"> Editar Usuario </h1>


<div class="col-md-12">
 {{ Form::open(['url' => '/usuario/persist', 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal form-bordered', 'role' => 'form']) }}
      <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Dados de usuários
                </h4>
            </div>
            <div class="panel-body">
                @if(!empty($data) && isset($data['usuario']))
                  <input class="form-control" type="hidden" value="{{$data['usuario']->id}}" name="id" />
                @else
                  <input class="form-control" type="hidden" value="" name="id" />
                @endif

                <div class="form-group col-md-6" style="min-height: 72px;overflow: hidden;">
                    <label class="control-label col-md-4 ui-sortable">
                        Acesso ao Admin
                    </label>

                    <div class="col-md-8 ui-sortable">
                        @if(!empty($data) && isset($data['usuario']) && $data['usuario']->b_admin)
                        <input type="checkbox" data-render="switchery" name="b_admin" checked/>
                        @else
                        <input type="checkbox" data-render="switchery" name="b_admin" />
                        @endif
                    </div>
                </div>

                <div class="form-group col-md-6" style="min-height: 72px;overflow: hidden;">
                    <label class="control-label col-md-4 ui-sortable">
                        Ativo
                    </label>

                    <div class="col-md-8 ui-sortable">
                        @if(!empty($data) && isset($data['usuario']) && $data['usuario']->b_ativo)
                        <input type="checkbox" data-render="switchery" name="b_ativo"  checked/>
                        @else
                        <input type="checkbox" data-render="switchery" name="b_ativo"  />
                        @endif
                    </div>
                </div>

                <div class="form-group col-md-6" style="min-height: 72px;overflow: hidden;">
                    <label class="control-label col-md-4 ui-sortable">
                        E-mail
                    </label>

                    <div class="col-md-8 ui-sortable">
                        @if(!empty($data) && isset($data['usuario']))
                            <input type="text" class="form-control" placeholder="Digite o E-mail do usuário" name="email" value="{{$data['usuario']->email}}" disabled/>
                        @else
                         <input type="text" class="form-control" placeholder="Digite o E-mail do usuário" name="email" value="" />
                        @endif
                    </div>
                </div>

                <div class="form-group col-md-6" style="min-height: 72px;overflow: hidden;">
                    <label class="control-label col-md-4 ui-sortable">
                        Nivel
                    </label>

                    <div class="col-md-8 ui-sortable">
                        <select name="nivel" class="form-control">
                            <option value="0" selected>Nenhum</option>
                            @foreach(\App\ScNivel::get() as $nivel)
                              @if(isset($data['usuario']))
                                @if($nivel->id == $data['usuario']->id_sc_nivel)
                                  <option value="{{$nivel->id}}" selected>{{$nivel->nome}}</option>
                                @else
                                  <option value="{{$nivel->id}}">{{$nivel->nome}}</option>
                                @endif
                              @else
                                <option value="{{$nivel->id}}">{{$nivel->nome}}</option>
                              @endif
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group col-md-6" style="min-height: 72px;overflow: hidden;">
                    <label class="control-label col-md-4 ui-sortable">
                        Senha
                    </label>

                    <div class="col-md-8 ui-sortable">
                        <input type="password" class="form-control" name="password" />
                    </div>
                </div>

                <div class="form-group col-md-6" style="min-height: 72px;overflow: hidden;">
                    <label class="control-label col-md-4 ui-sortable">
                        Confirma Senha
                    </label>

                    <div class="col-md-8 ui-sortable">
                        <input type="password" class="form-control" name="password_confirmation" />
                    </div>
                </div>
            </div>
    </div>

    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Açoes</h4>
        </div>
        <div class="panel-body">
            <div class="row" style="margin-top:20px;">
                <div class="col-md-4">
                    <button type="submit" class="btn btn-success">Salvar</button>
                </div>
            </div>
        </div>
      </div>
  </form>
</div>
</div>
@stop
