<div class="modal fade in" id="modal-editor" style="background: rgba(0,0,0,0.5);">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close close-modal" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title"></h4>
			</div>

            <div class="modal-body"></div>

			<div class="modal-footer">
				<a href="javascript:;" class="btn btn-sm btn-white close-modal"  data-dismiss="modal">Fechar</a>
				<a href="javascript:;" class="btn btn-sm btn-success save">Salvar</a>
			</div>
		</div>
	</div>
</div>
