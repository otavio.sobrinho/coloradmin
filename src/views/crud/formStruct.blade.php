<?php
use \locknload\Admin\Macros;
use \locknload\Crud\Helper;

$lastKey = "";
?>

<style>

.bootstrap-datetimepicker-widget{
  z-index:100 !important;
}

</style>

<div class="col-md-12">

    <div class="panel panel-inverse">

        @foreach ($data['columns'] as $container)
            <div class="panel-heading">
                <h4 class="panel-title">
                    {{!empty($container['container_header'])?$container['container_header']:"Configuração básica"}}
                </h4>
            </div>
            <div class="panel-body">
                <input class="form-control" type="hidden" value="{{$data['obj']['id']}}" name="id" />
                @foreach ($container as $fieldk => $field)

                    @if ($fieldk != 'container_header')
                    <div class="form-group col-md-<?= isset($field['form_size']) && $field['form_size'] == 'big' ? "12" : "6"?>" style="min-height: 72px;">
                        <label class="control-label col-md-<?= isset($field['form_size']) && $field['form_size'] == 'big' ? "2" : "4"?> ui-sortable">
                            {!!Macros::generateLabel(isset($field['label'])? $field['label']:$field['field'], isset($field['nullable']) ? $field['nullable'] : "yes" )!!}
                        </label>

                        <div class="col-md-<?= isset($field['form_size']) && $field['form_size'] == 'big' ? "10" : "8"?> ui-sortable">
                            @if ( $field['type'] == 'multiselect' )
                                {!!Macros::getMultiSelect($field, $data)!!}
                            @elseif ( $field['type'] == 'select' )
                                {!!Macros::getSelect($field, $data)!!}
                            @elseif ( $field['type'] == 'date' )
                                {!!Macros::getDate($field, $data)!!}
                            @elseif ( $field['type'] == 'image' )
                                {!!Macros::getFile($field)!!}
                            @elseif ( $field['type'] == 'color' )
                                {!!Macros::getColor($field, $data)!!}
                            @elseif ( $field['type'] == 'bool' )
                                {!!Macros::getCheckBox($field, $data, array("data-render"=>"switchery"))!!}
                            @elseif ( $field['type'] == 'text' )
                                {!!Macros::getTextArea($field, $data, array("style" => "width:100%"))!!}
                            @else
                                {!!Macros::getText($field, $data)!!}
                            @endif
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
        @endforeach

    </div>

    <div class="panel panel-inverse">

        <div class="panel-heading">
            <h4 class="panel-title">Açoes</h4>
        </div>

        <div class="panel-body">
            <div class="row" style="margin-top:20px;">
                <div class="col-md-4">
                    <button type="submit" class="btn btn-success">Salvar</button>
                </div>

                <div class="col-md-4 pull-right">
                    <a class="btn btn-danger pull-right"  href="{{ $data['obj']['id'] }}">Deletar</a>
                </div>
            </div>
        </div>

    </div>

</div>
