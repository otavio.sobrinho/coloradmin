<?php
use \locknload\Admin\Macros;
use \locknload\Crud\Helper;
use \locknload\Crud\Fields;

$lastKey = "";
?>

<div class="col-md-12">

    <div class="panel panel-inverse" data-sortable-id="form-stuff-2">

        <div class="panel-heading">

            @if($data['class'] == "ed_materia")
                <h4 class="panel-title">Informações de destaque</h4>
            @else
                <h4 class="panel-title">Informações básicas</h4>
            @endif

        </div>

        <div class="panel-body">

            <input class="form-control" type="hidden" value="{{$data['obj']['id']}}" name="id" />

            <div class="row">

                @foreach ($data['columns'] as $row)

                    @if ($row['field'] != "id")

                            <div class="form-group col-md-<?=Fields::isImage($row) || Fields::isText($row)?"12":"6"?>">

                                <label class="control-label col-md-4 ui-sortable">
                                    {!!Macros::generateLabel(isset($row['label'])? $row['label'] : $row['field'],$row['nullable'])!!}
                                </label>

                                <div class="col-md-8 ui-sortable">

                                    @if ( Fields::isSelect($row) )
                                        {!!Macros::getSelected($row, $data)!!}
                                    @elseif ( Fields::isDate($row) )
                                        {!!Macros::getDate($row, $data)!!}
                                    @elseif ( Fields::isImage($row) )
                                        {!!Macros::getFile($row)!!}
                                    @elseif ( Fields::isColor($row) )
                                        {!!Macros::getColor($row, $data)!!}
                                    @elseif ( Fields::isBool($row) )
                                        {!!Macros::getCheckBox($row, $data, array("data-render"=>"switchery"))!!}
                                    @elseif ( Fields::isText($row) )
                                        {!!Macros::getTextArea($row, $data, array("style" => "width:100%"))!!}
                                    @else
                                        {!!Macros::getText($row, $data)!!}
                                    @endif

                                </div>

                            </div>

                    @endif

                @endforeach

            </div>

        </div>

        @if(sizeof(Helper::getManyToMany($data['class'])) > 0 )
            <div class="panel-heading">
                <h4 class="panel-title">Outras informações</h4>
            </div>
            <div class="panel-body">

                @foreach(Helper::getManyToMany($data['class']) as $item)
                   <div class="form-group col-md-6">
                        <label class="control-label col-md-4 ui-sortable">
                            {!!Macros::generateLabel($item, null)!!}
                        </label>

                        <div class="col-md-8 ui-sortable">
                            <select multiple="multiple" name="multi_{{Macros::generateLabel($item, null)}}[]" class="default-select2 form-control">
                                @foreach (Helper::getManyToManyOptions($item, $data['class'], $data['obj']['nome']) as $key => $group)
                                    @if(is_string($key) && $key != $lastKey)
                                        @if ($key == "root")
                                            <optgroup label="Raiz">
                                        @else
                                            <optgroup label="{{$key}}">
                                        @endif
                                    @endif

                                    @foreach($group as $option)
                                        @if(Helper::itemHasOption($data['class'], Macros::generateLabel($item, null), $data['obj']['id'], $option->id))
                                            <option value="{{$option->id}}" selected> {{$option->nome}} </option>
                                        @else
                                            <option value="{{$option->id}}"> {{$option->nome}} </option>
                                        @endif
                                    @endforeach

                                    @if(is_string($key) && $key != $lastKey)
                                        </optgroup>
                                    @endif

                                    @if ($loop->last)
                                        </optgroup>
                                    @endif

                                    <?php $lastKey = $key; ?>
                                @endforeach
                            </select>
                        </div>
                   </div>
                @endforeach

            </div>
        @endif

        <div class="panel-heading">
            <h4 class="panel-title">Açoes</h4>
        </div>

        <div class="panel-body">
            <div class="row" style="margin-top:20px;">
                <div class="col-md-4">
                    <button type="submit" class="btn btn-success">Salvar</button>
                </div>

                <div class="col-md-4 pull-right">
                    <a class="btn btn-danger pull-right"  href="{{ $data['obj']['id'] }}">Deletar</a>
                </div>
            </div>
        </div>

    </div>

</div>
