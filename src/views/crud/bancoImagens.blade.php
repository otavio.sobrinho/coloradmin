@if($data['imagens'])
<ul id="bancoImagens" style="list-style:none"><li style="float:left"><a href="0">Nenhuma Foto</a></li>
    @foreach($data['imagens'] as $imagem)
        <li style="float:left">
            <a href="{{$imagem->id}}">
                <img src="{{$imagem->imagem}}"  width = "100px" />
            </a>
        </li>
    @endforeach
</ul>
{!! $data['imagens']->links() !!}
@endif

