<?php
use App\ScMenu as ScMenu;
use LocknLoad\Crud\Helper as Helper;
?>
<div id="sidebar" class="sidebar">

	<div data-scrollbar="true" data-height="100%">
		<a href="/">
			<ul class="nav">

				<li class="nav-profile">

					<div class="image">

						@if(!empty(Auth::user()->perfil->img_perfil))
							<img src="{{Auth::user()->perfil->img_perfil}}" alt="Avatar">
						@else
							<i class="fa fa-user" style="margin-left:10px;  margin-top:5px;"></i>
						@endif

					</div>

					<div class="info">

						@if(isset(Auth::user()->perfil->nome))
			                {{Auth::user()->perfil->nome}}
			            @else
				            {{Auth::user()->email}}
		                @endif

						<small> {{Auth::user()->nivell}}</small>

					</div>

				</li>

			</ul>
		</a>

        <ul class="nav">

            @foreach(ScMenu::getRoot() as $menu)
                @if($menu->access_level <= Auth::user()->level())
                    <li class="nav-header">{{$menu->label}}</li>

                    @foreach($menu->getSubmenu($menu->id) as $secondLevel)
                        @if($secondLevel->type == 'dropdown')
                           @if($secondLevel->access_level <= Auth::user()->level())
                            <li class="has-sub">
                                <a href="javascript:;">
                                    <b class="caret pull-right"></b>
                                    <i class="fa {{$secondLevel->icon}}"></i>
                                    <span>{{$secondLevel->label}}</span>
                                </a>

                                <ul class="sub-menu">
                                    @foreach($secondLevel->getSubmenu($secondLevel->id) as $thirdLevel)

                                        @if($thirdLevel->type == "submenu")
                                            @foreach(Helper::generateModel($thirdLevel->model)::get() as $item)
                                               @if($item->access_level <= Auth::user()->level())
                                                <li>
                                                    <a href="/editar/{{snake_case($thirdLevel->model)}}/{{$item->id}}">
                                                        {{$item->nome}}
                                                    </a>
                                                </li>
                                                @endif
                                            @endforeach
                    	  										@if($thirdLevel->access_level <= Auth::user()->level())
                                              <li>
                                                <a href="/listar/{{snake_case($thirdLevel->model)}}">
                                                    {{$thirdLevel->label}}
                                                </a>
                                              </li>
											                      @endif
                                        @else
											                    @if($thirdLevel->access_level <= Auth::user()->level())
                                            <li>
                                                <a href="/listar/{{snake_case($thirdLevel->model)}}">
                                                    {{$thirdLevel->label}}
                                                </a>
                                            </li>
											                    @endif
                                        @endif

                                    @endforeach
                                </ul>
                            </li>
                            @endif
                            @elseif($secondLevel->type == 'item')
                              @if($secondLevel->access_level <= Auth::user()->level())
                                <li>
                                    @if(isset($secondLevel->b_custom) && $secondLevel->b_custom == true)
                                      <a href="{{$secondLevel->model}}">
                                    @else
                                      <a href="/listar/{{snake_case($secondLevel->model)}}">
                                    @endif 
                                    <i class="fa {{$secondLevel->icon}}"></i>
                                    <span>{{$secondLevel->label}}</span>
                                  </a>
                                </li>
                              @endif
                           @endif
                    @endforeach
                @endif
            @endforeach

        </ul>

    </div>

</div>

<div class="sidebar-bg"></div>
