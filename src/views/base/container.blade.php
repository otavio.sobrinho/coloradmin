<?php use Illuminate\Support\Facades\Auth;
use \LocknLoad\Crud\Helper;
use \Locknload\Admin\Macros;
use \LocknLoad\Crud\Fields;
?>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/lib/Ionicons/css/ionicons.css" rel="stylesheet">

    @yield('style')

    <link rel="stylesheet" href="/css/slim.css">

  </head>
  <body>
    @auth
      <div class="slim-header">
        <div class="container">
        <div class="slim-header-left">
          <h2 class="slim-logo"><a href="/">{{ config('app.name', 'Laravel') }}</a></h2>
        </div><!-- slim-header-left -->
        <div class="slim-header-right">
          <div class="dropdown dropdown-c">
          <a href="#" class="logged-user" data-toggle="dropdown">
            <img src="http://via.placeholder.com/500x500" alt="">
            <span>{{ Auth::user()->name }}</span>
            <i class="fa fa-angle-down"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right">
            <nav class="nav">
              <a href="page-profile.html" class="nav-link"><i class="icon ion-person"></i> View Profile</a>
              <a href="page-edit-profile.html" class="nav-link"><i class="icon ion-compose"></i> Edit Profile</a>
              <a href="page-activity.html" class="nav-link"><i class="icon ion-ios-bolt"></i> Activity Log</a>
              <a href="page-settings.html" class="nav-link"><i class="icon ion-ios-gear"></i> Account Settings</a>
              <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="icon ion-forward"></i> {{ __('Logout') }}
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>
            </nav>
          </div><!-- dropdown-menu -->
          </div><!-- dropdown -->
        </div><!-- header-right -->
        </div><!-- container -->
      </div><!-- slim-header -->
      <div class="slim-navbar">
        <div class="container">
          <ul class="nav">
            <li class="nav-item with-sub">
              <a class="nav-link" href="#">
                <i class="icon ion-ios-home-outline"></i>
                <span>Visão Geral</span>
              </a>
              <div class="sub-item">
                <ul>
                  <li><a href="/">Todas as empresas</a></li>
                </ul>
              </div><!-- sub-item -->
            </li>
            <li class="nav-item with-sub">
              <a class="nav-link" href="#">
                <i class="icon ion-ios-filing-outline"></i>
                <span>Vendas</span>
              </a>
              <div class="sub-item">
                <label class="section-label">Controle</label>
                <ul>
                  <li><a href="/caixa">Caixa</a></li>
                </ul>
                <label class="section-label">Cadastros</label>
                <ul>
                  <li><a href="/customers">Clientes</a></li>
                  <li><a href="/products">Produtos</a></li>
                </ul>
              </div>
            </li>
            <li class="nav-item with-sub">
              <a class="nav-link" href="#">
                <i class="icon ion-ios-filing-outline"></i>
                <span>Compras</span>
              </a>
              <div class="sub-item">
                <label class="section-label">Cadastros</label>
                <ul>
                  <li><a href="/">Fornecedores</a></li>
                  <li><a href="form-elements.html">Produtos</a></li>
                  <li><a href="form-elements.html">Estoque</a></li>
                </ul>
              </div>
            </li>
            <li class="nav-item with-sub">
              <a class="nav-link" href="#">
                <i class="icon ion-ios-filing-outline"></i>
                <span>Financeiro</span>
              </a>
              <div class="sub-item">
                <label class="section-label">Movimentações</label>
                <ul>
                  <li><a href="/payable">Contas a pagar</a></li>
                  <li><a href="/receivable">Contas a receber</a></li>
                </ul>
                <label class="section-label">Cadastros</label>
                <ul>
                  <li><a href="form-elements.html">Categorias</a></li>
                </ul>
              </div>
            </li>
          <li class="nav-item">
          <a class="nav-link" href="page-messages.html">
            <i class="icon ion-ios-chatboxes-outline"></i>
            <span>Relatórios</span>
            <span class="square-8"></span>
          </a>
          </li>
        </ul>
        </div><!-- container -->
      </div>
      <div class="slim-mainpanel">
        <div class="container">
          <div class="slim-pageheader">
            <ol class="breadcrumb slim-breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">Pages</a></li>
              <li class="breadcrumb-item active" aria-current="page">Profile Page</li>
            </ol>
            <h6 class="slim-pagetitle">{{Helper::translateField( $data['class'] )}}</h6>
          </div><!-- slim-pageheader -->

    @else
    <div class="signin-wrapper">
    @endauth

    @yield('conteudo')

    @auth
      </div>
    </div>
    @else
    </div>
    @endauth

    <div class="slim-footer">
      <div class="container">
        <p>Copyright 2018 &copy; All Rights Reserved.</p>
        <p><a href="">⁂</a></p>
      </div><!-- container -->
    </div><!-- slim-footer -->


    <script src="/lib/jquery/js/jquery.js"></script>
    <script src="/lib/popper.js/js/popper.js"></script>
    <script src="/lib/bootstrap/js/bootstrap.js"></script>

    @yield('script')

    <script src="/js/slim.js"></script>

  </body>
</html>