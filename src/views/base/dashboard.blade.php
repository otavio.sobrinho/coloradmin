@extends('admin::base.container')

@section('style')
    <link href="/assets/css/dashboard-{{env('ASSETS_VERSION')}}.min.css" rel="stylesheet" />
@stop

@section('conteudo')

<div id="content" class="content">

	<ol class="breadcrumb pull-right">
		<li><a href="javascript:;">Home</a></li>
		<li><a href="javascript:;">Dashboard</a></li>
		<li class="active">Dashboard v2</li>
	</ol>

	<h1 class="page-header">Dashboard v2 <small>header small text goes here...</small></h1>

	<div class="row">

	    <div class="col-md-3 col-sm-6">

	        <div class="widget widget-stats bg-green">

	        </div>

	    </div>

	    <div class="col-md-3 col-sm-6">

	        <div class="widget widget-stats bg-blue">
	            <div class="stats-icon stats-icon-lg"><i class="fa fa-users fa-fw"></i></div>
	            <div class="stats-title">Leads Cadastrados</div>
              @if (isset($leads['quantidade']))
	              <div class="stats-number">{{$leads['quantidade']}}</div>
              @endif
	            <div class="stats-progress progress">
                    @if(isset($leads['porcent']))
                      <div class="progress-bar" style="width: {{$leads['porcent']}}%;"></div>
                    @endif
              </div>
              @if(isset($leads['porcent']))
                <div class="stats-desc">{{$leads['porcent']}}% a mais neste mes</div>
              @endif
	        </div>



	    </div>

	    <div class="col-md-3 col-sm-6">

	        <div class="widget widget-stats bg-purple">

	       </div>

	    </div>

	    <div class="col-md-3 col-sm-6">

	        <div class="widget widget-stats bg-black">

	        </div>

	    </div>

	</div>

    <!-- begin row -->
    <div class="row">
        <div class="col-md-8">
            <div class="widget-chart with-sidebar bg-black">
                <div class="widget-chart-content">
                    <h4 class="chart-title">
                        Google Analytics
                        <small>Audiencia último 4 meses</small>
                    </h4>
                    <div id="visitors-line-chart" class="morris-inverse" style="height: 260px;"></div>
                </div>
                <div class="widget-chart-sidebar bg-black-darker">
                    <div class="chart-number">
                        @if(isset($YesterdayViews) && sizeof($YesterdayViews) > 0)
                          {{$YesterdayViews[0][0]}} / {{$YesterdayViews[0][1]}}
                          <small>Usuários / Pageviews</small>
                        @endif
                    </div>
                    <div id="visitors-donut-chart" style="height: 160px"></div>
                    <ul class="chart-legend">
                      @if(isset($NewReturnUsers) && sizeof($NewReturnUsers) > 0)
                        <li><i class="fa fa-circle-o fa-fw text-success m-r-5"></i> {{ number_format( ($NewReturnUsers[0][1] * 100) / $NewReturnUsers[0][0], 2)}} %  <span>Novos</span></li>
                        <li><i class="fa fa-circle-o fa-fw text-primary m-r-5"></i> {{ number_format( ( ($NewReturnUsers[0][0]-$NewReturnUsers[0][1]) * 100 ) / $NewReturnUsers[0][0], 2)}} % <span>Retornaram</span></li>
                      @endif
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-inverse" data-sortable-id="index-1">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Origem das visitas
                    </h4>
                </div>
                <div class="list-group" style="overflow-x: hidden; max-height: 295px; overflow-y: auto;">
                @foreach ($PontosAcesso as $key => $ponto)
                    <a href="#" class="list-group-item list-group-item-inverse text-ellipsis">
                        <span class="badge badge-success">{{$ponto[1]}}</span>
                        {{$ponto[0]}}
                    </a>
                @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-inverse" data-sortable-id="flot-chart-3">
                <div class="panel-heading">
                    <h4 class="panel-title">Categorias mais acessadas</h4>
                </div>
                <div class="panel-body">
                    <div id="morris-bar-chart" class="morris-bar-chart height-sm"></div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-inverse" data-sortable-id="index-1">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        Materias mais acessadas
                    </h4>
                </div>
                <div class="list-group" style="overflow-x: hidden; max-height: 325px; overflow-y: auto;">
                @foreach ($materiasMaisAcessadas as $key => $item)
                    <a href="" class="list-group-item list-group-item-inverse text-ellipsis">
                        <span class="badge badge-success">{{$item->acessos}}</span>
                        {{$item->titulo}}
                    </a>
                @endforeach
                </div>
            </div>
        </div>

    </div>
</div>

<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>

@stop

@section('script')
    <script src="{{ URL::asset('/assets/js/dashboard-'.env('ASSETS_VERSION').'.min.js') }}"></script>

    <script>
  //    var jsonlinechart = [{--!!$jsonlinechart!!--}];
        var jsonlinechart = null;
        var chartRetornaram = null;
        var chartNovos = null;
        var chartMaisAcessados = [

            @foreach($maisAcessados as $key => $item)
                @if ($key > 0)
                    ,
                @endif
                {categoria: "{{$item->nome}}", acessos: "{{$item->acessos}}"}
            @endforeach
        ];

        $(document).ready(function() {
            DashboardV2.init();
        });
    </script>
@stop
