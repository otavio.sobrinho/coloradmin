<?php

namespace LocknLoad\Admin;

use Illuminate\Console\Command;

class Comands extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'adminAssets:create';

    /**
     * The console command description.
     *ra
     * @var string
     */
    protected $description = 'Minifica e gera o uglify dos assets do tema admin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $version = env('ASSETS_VERSION');

        echo "\r\nuglify e unindo arquivos js";
        $dirs = array_filter(glob(__DIR__.'/assets/js/*'), 'is_dir');

        foreach ($dirs as $dir){
            $name = explode("/",$dir)[sizeof(explode("/",$dir))-1];
            exec('uglifyjs '.__DIR__.'/assets/js/'.$name.'/*.js -c -o public/assets/js/admin-'.$name.'-'.$version.'.min.js');
        }


    }
}
