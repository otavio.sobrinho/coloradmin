<?php
namespace locknload\Admin;

use LocknLoad\Crud\Factory as SuperFactory;
use LocknLoad\Crud\Service;
use LocknLoad\Crud\Helper;

/**
 * Macros
 *
 * @uses SuperFactory
 * @package Locknload.coloradmin
 * @version //autogen//
 * @copyright LocknLoad technology (http://www.lockload.me) 2016 All rights reserved.
 * @author Davi Menegotto (supmouse)
 * @license PHP Version 3.0 {@link http://www.php.net/license/3_0.txt}
 *
 * @description This class must be resposable for return html tags for views that shows forms or list of models
 */
class Macros extends SuperFactory
{

    /* MACROS */

    public static function getTextArea($field, $data, array $extra = array()){

        $attrs = Service::generateExtra($field, $extra);

        if ( isset($field['plain-text']) && $field['plain-text'] )
            $content = "<div class=\"form-group col-md-12\"><textarea name=\"{$field['field']}\" rows=\"20\" {$attrs} >{$data['obj'][$field['field']]}</textarea></div>";
        else
            $content = "<div class=\"form-group col-md-12\"><textarea name=\"{$field['field']}\" class=\"wysihtml5\" rows=\"20\" {$attrs} >{$data['obj'][$field['field']]}</textarea></div>";

        return $content;

    }

    public static function getColor($field,$data){
        return "<div class=\"input-group colorpicker-component\" data-color=\"{$field['field']}\" data-color-format=\"rgb\"  id=\"colorpicker-prepend\">
                    {$this->getText($row, $data)}
                    <span class=\"input-group-addon\"><i></i></span>
                </div>";
    }

    public static function getSelect($field, $data, array $extra = array()){
        $attrs    = Service::generateExtra($field, $extra);
        $value    = Service::getRelation($field, $data);
        $content  = "<select name=\"{$field['field']}\" class=\"form-control\" {$attrs} ><option value=\"0\">Nenhum</option>";


        if (!$value && isset($field['enum'])) $value = $field['enum'];

        if (sizeof($value) > 0 ) {
            if ( isset($field['enum']) && !empty($data['obj'][$field['field']]) ){
                $content .= "<option value=\"{$data['obj'][$field['field']]}\" selected>{$data['obj'][$field['field']]}</option>";
            } else if ( isset($data['obj'][$field['field']]) ) {
                $model        = Helper::generateModel($field['field']);
                $itemSelected = $model::find($data['obj'][$field['field']]);
                if($itemSelected) $content     .= "<option value=\"{$data['obj'][$field['field']]}\" selected>{$itemSelected->presentation()}</option>";
            } else if ( isset($field['default']) ) $content .= "<option value=\"{$field['default']}\" selected>{$field['default']}</option>";

            foreach($value as $op) {
                if (isset($field['enum']) && !empty($field['enum']) ) $content .= "<option value=\"{$op}\">{$op}</option>";
                else $content .= "<option value=\"{$op->id}\">{$op->presentation()}</option>";
            }
        }

        $content .= "</select>";

        return $content;
    }

    public static function getMultiSelect($field, $data, array $extra = array()){
        $name    = preg_replace('/id_/','',$field['field']);
        $method  = explode('_',$field['field']);
        $method  = $method[sizeof($method)-1];
        $content = "<select multiple=\"multiple\" name=\"multi_{$name}[]\" class=\"default-select2 form-control\">";
        $options = Helper::getManyToManyOptions($field);
        $itemns  = null;

        if (sizeof($data['obj']->$method) > 0) {
            foreach ($data['obj']->$method as $item){
                $itemns[] = $item->presentation();
            }
        }

        foreach ( $options as $key => $group ) {

            foreach($group as $id => $option) {
                $content .= "<optgroup label=\"{$option['item']}\">";

                if (is_array($itemns) && in_array($option['item'],$itemns)) $content .= "<option value=\"{$id}\" selected=\"selected\"> {$option['item']} </option>";
                else $content .= "<option value=\"{$id}\"> {$option['item']} </option>";

                if (sizeof($option['subitem'])) {
                    foreach ($option['subitem'] as $subid => $subitem){
                        if (is_array($itemns) && in_array($subitem,$itemns)) $content .= "<option value=\"{$subid}\" selected=\"selected\"> {$subitem} </option>";
                        else $content .= "<option value=\"{$subid}\"> {$subitem} </option>";
                    }
                }

                $content .= "</optgroup>";
            }

        }

        $content .= "</select>";

        return $content;

    }

    public static function getText($field, $data, array $extra = array()){
        $attrs = Service::generateExtra($field, $extra);
        $value = Service::getRelation($field, $data);

        if (!$value) $value = !empty($data['obj'][$field['field']]) ? $data['obj'][$field['field']] : $field['default'];

        return "<input type=\"text\" class=\"form-control\" placeholder=\"{$field['field']}\" name=\"{$field['field']}\" value=\"{$value}\" {$attrs} />";
    }

    public static function getCheckBox($field, $data, $extra = null){

        $attrs = Service::generateExtra($field, $extra);
        if ( !empty($data['obj'][$field['field']]) ) return "<input type=\"checkbox\" name=\"{$field['field']}\" {$attrs} checked />";
        else return "<input type=\"checkbox\" name=\"{$field['field']}\" {$attrs} />";

    }

    public static function getFile($row, array $extra = array()){
        $attrs = Service::generateExtra($row, $extra);
        return "<a id=\"trigger-banco-imagens\" href=\"#banco-imagens\" class=\"banco-imagens col-md-5\" data-toggle=\"modal\">
                    <i style=\"font-size: 30px; margin-right:10px;\" class=\"fa fa-picture-o\"></i><span>Clique aqui para selecionar uma imagem</span>
                </a>
                <div class=\"col-md-2\"><b>OU add nova imabem</b></div>
                <div class=\"col-md-5\">
                    <input class=\"form-control\" type=\"file\" value=\"\" name=\"{$row['field']}\"  placeholder=\"{$row['field']}\" {$attrs} />
                </div>";
    }

    public static function showListImage($row, $col, array $extra = array()){
        $attrs = Service::generateExtra($row, $extra);
        if(!empty($row[$col['field']])) {
            if($col['field'] == env('CRUD_IMG_FIELD',"id_gb_image") ) {
                if (!empty($row[$col['field']])) {
                    $obj = \App\GbImage::find($row[$col['field']]);

                    if ($obj) return "<img src=\"{$obj->imagem}\" {$attrs} />";
                    else return "<i class=\"fa fa-file-image-o\" style=\"font-size:20px\"></i>";

                } else {
                    return "<i class=\"fa fa-file-image-o\" style=\"font-size:20px\"></i>";
                }
            } else {
                return "<img src=\"{$row[$col['field']]}\" {$attrs} />";
            }
        }
    }

    public static function showListColor($row, $col){
        return "<span style=\"width:40px; height:40px; display:block; border-radius:4px; background-color: {$row[$col['field']]} \"> </span>";
    }

    public static function showListBool($row, $col){
        if( $row[$col['field']] == 1) return "<span class=\"fa-stack fa-2x text-success\"> <i class=\"fa fa-check\"></i> </span>";
        else return "<span class=\"fa-stack fa-2x text-danger\"> <i class=\"fa fa-ban\"></i> </span>";
    }

    public static function getDate($row,$data){
        return "<div class=\"input-group date\" id=\"datetimepicker_{$row['field']}\">"
                   .Self::getText($row, $data). 
                    "<span class=\"input-group-addon\">
                        <span class=\"glyphicon glyphicon-calendar\"></span>
                    </span>
                </div>";

    }

}
